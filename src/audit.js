import {
    audit,
    distinctUntilChanged,
    map
} from "rxjs/operators";
import {
    fromEvent,
    interval,
    timer
} from "rxjs";
export const auditDemo = () => {
    const valueChanges = fromEvent(document.getElementById('search'), 'keydown')
    const result = valueChanges.pipe(
        /**
         * Take the last event 
         * after the first and untill
         * the timer is disabled
         */
        audit(ev => timer(1000)),
        /**
         * Get its target value
         */
        map(x => x.target.value),
        /**
         * If the value has changed
         * then emit it
         */
        distinctUntilChanged()
    );
    result.subscribe(x => console.log(x));
}